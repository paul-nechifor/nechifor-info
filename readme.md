# Nechifor Info

The file that serves as the hub of all information about my work and me. It's
used for generating [my homepage][hp] and [my resume][cv].

[hp]: https://github.com/paul-nechifor/nechifor-index
[cv]: https://github.com/paul-nechifor/cv
